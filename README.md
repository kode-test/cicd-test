# Реализация
Что я сделал:

1. Сначала подумал как, в общем, будет выглядеть docker-compose, потому что мы его деплоим на прод.
Решил, что доступ к приложениям будет закрытый (Django, PostgreSQL). А весь трафик через nginx

2. Поэтому написал простой конфиг для nginx,  чтобы трафик шел на наше приложение 

3. Dockerfile был готовый, я его трогать не стал, но в теории можно был сделать multistage сборку для уменьшения размера

4. Поправил docker-compose с учетом nginx и с условием, того что мы будем деплоить наш compose. 
Прописал путь на image нашего app, в целом, была только настройка nginx, чтобы брал конфиг с репы. 
В идеале, конечно, db следует отдельно держать, точно не в контейнере, но для теста можно))

5. CI/CD по факту, у нас всего 2 джобы, сборка образа и деплой. сборку сделал с возможностью сразу добавить еще образов, чтобы не пришлось писать новый код)) А деплой, просто запуск compose файла, но там еще заменил latest compose, на свой, чтобы смог запустить на своей машине.
А в идеале pipeline выглядел бы так:

- коммит -> тригер билд -> билд -> тесты -> деплой на стейдж -> review -> деплой на прод

6. А еще я под вечер тупил, поэтому бренчу не поменял? а лучше сначала условно на dev, куча глупых ошибок, когда pipeline запускал, главное не ругайтесь))



## Django with PostgreSQL

 взял этот проект, для примера: https://github.com/thejungwon/docker-webapp-django


One-minute deployment, simple web-application.

*It is not recommended to deploy a core database as a container. This example shows how to handle the multi-container situation, when one container (Django) strongly depends on the other container (database).*


## Getting Started
![Screen Shopt](images/main-screenshot.png?raw=true "Screen Shot")
Two containers
  * web app(Django)
  * database(PostgreSQL)

If a container (Django) should be launched after another container(postgres) we can define it in the `depends_on` field.

```
version: '3.3'

services:
  app:
    build:
      context: ./src
      dockerfile: Dockerfile
    ports:
      - "8000:8000"
    depends_on:
      - db
  db:
    image: "postgres:13.5-alpine"
    ports:
      - "5432:5432"
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres

```




